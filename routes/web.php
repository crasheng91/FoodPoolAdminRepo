<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/orders', 'OrdersController@viewOrders')->name('orders');
Route::post('/restaurant', 'RestaurantController@create')->name('createRestaurant');


Route::post('/update-restaurant', 'RestaurantController@update')->name('updateRestaurant');
Route::post('/update-order-status', 'OrdersController@update')->name('updateOrderStatus');
Route::post('update-dish-status', 'DishesController@update')->name('updateDishState');
Route::post('delete-dish', 'DishesController@delete')->name('deleteDish');

Route::post('/delete-restaurant', 'RestaurantController@delete')->name('deleteRestaurant');
Route::get('/restaurant/{id}/dish', 'DishesController@viewDishes')->name('viewDishes');
Route::post('/new-dish', 'DishesController@createDish')->name('createDish');

//Route::get('/restaurant', 'RestaurantController@create')->name('findRestaurant');
