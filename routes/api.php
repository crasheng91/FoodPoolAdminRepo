<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:api')->get('/restaurants', function (){
    $kitchens =  \App\Kitchen::all()->where('active','==', true);
//    $someArray = json_decode($kitchens, true);
    $kitchen_array = Array();
    foreach ($kitchens as $kitchen){
        array_push($kitchen_array, $kitchen);
    }
    return response()->json($kitchen_array, 200);
});

Route::middleware('auth:api')->get('/restaurants/{kitchen}', function (Request $request, \App\Kitchen $kitchen){
//    $id = $kitchen->id;
    $dishess=  $kitchen->dishes->where('active','==', true);
    $dish_array = Array();
    foreach ($dishess as $dish){
        array_push($dish_array, $dish);
    }
    return response()->json($dish_array, 200);
});


Route::middleware('auth:api')->put('/restaurants/rating/{id}', function(Request $request, \App\Kitchen $kitchen) {
//    $kitchen->rating = 8.8;
//    $kitchen->save();
    $kitchen->rating = $request->rating;
    $kitchen->save();
    return response()->json($kitchen, 200);
});



Route::middleware('auth:api')->get('/orders/', function(Request $request) {

    $user = $request->user();
    $orders = App\User::find($user->id)->orders;
    return response()->json($orders, 200);
});


Route::middleware('auth:api')->get('/orders/{id}', function(Request $request,$id) {

    $order_details = \App\Order::find($id)->detailDishes;
    return response()->json($order_details, 200);
});



//post an order from mobile user
Route::middleware('auth:api')->post('/orders/', function(Request $request) {


    $comingOrders = (array) json_decode($request->getContent());
    $receipt = 0;


    foreach ($comingOrders as $item){
        $receipt += $item->sub_total;
    }

    //get request user
    $user = $request->user();

    //create order in table
    $order =  new \App\Order();
    $order->status = false;
    $order->receipt = $receipt;
    $order->user_id = $user->id;
    $order->save();

    //add for intermediate table ( Pivot Table)
    foreach ($comingOrders as $item){
        $order->dishes()->attach($item->dish_id, [
            'qty' => $item->qty,
            'total_price'=> $item->sub_total,
            'created_at'=> new DateTime(), 'updated_at'=> new DateTime()]);
    }
    return response()->json($order, 200);
});


Route::post('register', 'Auth\RegisterController@register');

Route::post('login', 'Auth\LoginController@login');

Route::post('logout', 'Auth\LoginController@logout');
