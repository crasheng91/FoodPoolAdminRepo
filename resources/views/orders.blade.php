@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="refreshTable">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">All placed orders</h3>
                        </div>

                        <table id="mytable" class="table table-bordred table-striped">

                            <thead>
                            <th style="text-align: center; vertical-align: middle;">Customer</th>
                            <th style="text-align: center; vertical-align: middle;">Receipt</th>
                            <th style="text-align: center; vertical-align: middle;">Delivered</th>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td style="text-align: center; vertical-align: middle;"><a href="#">{{$order->user->name}}</a></td>
                                    <td style="text-align: center; vertical-align: middle;"><a href="#">{{$order->receipt}} SAR</a></td>

                                    <td style="text-align: center; vertical-align: middle;">
                                        <input type="hidden" id="orderId"  value="{{$order->id}}"/>
                                        @if($order->status == 1)
                                            <input type="checkbox" data-size= "mini" data-mine="{{$order->id}}" data-on-text="Delivered" data-off-text="Processing" name="order-checkbox" checked></td>
                                    @else
                                        <input type="checkbox" data-size= "mini"  data-mine="{{$order->id}}" data-on-text="Delivered" data-off-text="Processing" name="order-checkbox" unchecked></td>
                                    @endif
                                </tr>
                            @endforeach

                            </tbody>

                        </table>


                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection

