@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="sidebar-nav">
                    <div class="well" style="width:300px; padding: 8px 0;">
                        <ul class="nav nav-list">
                            <li><a href="{{url('home')}}"><i class="icon-home"></i> Restaurants</a></li>
                            <li><a href="{{url('orders')}}"><i class="icon-envelope"></i> orders <span class="badge badge-info">{{$ordersNo}}</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div id="refreshTable">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Restaurants<a href="#" class="pull-right" id="plusButtonID" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus" aria-hidden="true"></i></a></h3>
                    </div>
                    <div class="panel-body">
                        All the Available restaurants on the dashboard
                    </div>

                    <table id="mytable" class="table table-bordred table-striped">

                        <thead>

                        {{--<th><input type="checkbox" id="checkall" /></th>--}}
                        <th style="text-align: center; vertical-align: middle;">Restaurants Title</th>
                        <th style="padding-left: 10px;">Picture</th>
                        <th style="text-align: center; vertical-align: middle;">Dishes</th>
                        {{--<th style="text-align: center; vertical-align: middle;">Edit</th>--}}
                        <th style="text-align: center; vertical-align: middle;">Delete</th>
                        <th style="text-align: center; vertical-align: middle;">Disable</th>
                        </thead>
                        <tbody>
                        @foreach($kitchens as $Kitchen)
                            <tr>
                                {{--<td style="text-align: center; vertical-align: middle;"><input type="checkbox" class="checkthis" /></td>--}}
                                <td style="text-align: center; vertical-align: middle;"><a href={{url("restaurant/{$Kitchen->id}"."/dish")}}>{{$Kitchen->title}}</a></td>
                                <td style="text-align: center; vertical-align: middle;"><img  style="width:200px;height:100px;" src="/images/{{$Kitchen->picture}}" class="img-responsive"></td>
                                <td style="text-align: center; vertical-align: middle;">{{$Kitchen->dishes->count()}}</td>
                                {{--<td style="text-align: center; vertical-align: middle;">--}}
                                    {{--<p data-placement="top" data-toggle="tooltip" title="Edit">--}}
                                        {{--<button class="btn btn-primary btn-xs editClicked" data-editid="{{$Kitchen->id}}"--}}
                                                {{--data-toggle="modal" data-target="#myModal"  data-title="Edit">--}}
                                            {{--<i class="fa fa-pencil" aria-hidden="true"></i>--}}
                                        {{--</button></p></td>--}}
                                <td style="text-align: center; vertical-align: middle;">
                                    <p data-placement="top" data-toggle="tooltip" title="Delete">
                                        <button class="btn btn-danger btn-xs deleteClicked" data-title="Delete" data-deleteid="{{$Kitchen->id}}">
                                            <i class="fa fa-trash" aria-hidden="true"></i></button></p>
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    @if($Kitchen->active == 1)
                                        <input type="checkbox" data-size="mini"  data-mine="{{$Kitchen->id}}" name="rest-checkbox" checked></td>

                                @else
                                    <input type="checkbox"  data-mine="{{$Kitchen->id}}" data-size= "mini"  name="rest-checkbox" unchecked></td>

                                @endif
                            </tr>
                        @endforeach

                        </tbody>

                    </table>

                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">New restaurant</h4>
                                </div>
                                <div class="modal-body">
                                    <form enctype="multipart/form-data" name="my_form" class="form-label-left" id="my_form" action="{{ route('createRestaurant') }}" method="POST">
                                        <div class="form-group">
                                        <label class="col-md-6 control-label">Restaurant Title <span class="required">*</span></label>
                                    <input type="text" name="resTitle" placeholder="Title of Restaurant" id="restNameId" class="form-control"/>
                                    <br/>
                                    <input type="file" name="resphoto" id="restPhotoId" class="form-control"/>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <div class="pull-left form-control-feedback" id="errormessage"></div>
                                    <button type="button" class="btn btn-default" id="closeEditID" data-dismiss="modal" style="display: none;">Close</button>
                                    <button type="button" class="btn btn-primary" id="saveEditID" style="display: none;">Save changes</button>
                                    <button type="submit" class="btn btn-primary" id="addNewRestID" data-dismiss="modal">Add Restaurant</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                </div>

            </div>

        </div>
    </div>
@endsection
