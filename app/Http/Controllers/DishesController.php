<?php

namespace App\Http\Controllers;

use App\Kitchen;
use Illuminate\Http\Request;
use App\Dish;
class DishesController extends Controller
{
    //
    public function viewDishes($id){

        $kitchen = Kitchen::find($id);

//        dd($kitchen->dishes());

        return view('dishs',[
            'dishes' => $kitchen->dishes,
            'kitchen_name' =>$kitchen->title,
            'kitchen_id' => $kitchen->id,
        ]);
    }

    public function createDish(Request $request){

        $dishTitle = $request->dishName;
        $kitchen = Kitchen::find($request->kitchen_id);
        $dishPrice = $request->kitchen_price;

        error_log($dishTitle, 4);
        $filename = "";
        if ($request->file('dishPhoto')->isValid()){
            $filename = time()."dish".'.'.$request->file('dishPhoto')->getClientOriginalName();
            $request->dishPhoto->move(public_path('images'), $filename);
        }


        $dish = new Dish();
        $dish->title = $dishTitle;
        $dish->picture = $filename;
        $dish->price = $dishPrice;

        //Assign the kitchen id to the created dish
        $kitchen->dishes()->save($dish);


        //        $kitchen->active = true;
        $result = array('status' => "Done");

        return json_encode($result);
    }

    public function update(Request $request){
        if ($request->id != ""){
            $dish = Dish::find($request->id);
            $dish->active = ($request->status) == "false" ? false: true;
            $dish->save();
        }
    }

    public function delete(Request $request){
        if ($request->id != ""){
            $dish = Dish::find($request->id);
            $deleted = $dish->delete();
            return $deleted == true ? self::deleteImageFromServer($dish->picture): "Something went bad in DB";
        }
    }


    private function deleteImageFromServer($filename){
//        $image_path = public_path('images')."/".$filename;  // Value is not URL but directory file path
        $image_path = public_path().'/images/'.$filename;
        error_log($image_path);
        if(file_exists($image_path)) {
//            error_log(unlink($image_path));
            unlink($image_path);
            return "File deleted successfully";
        }
        return "File not on server bad record";
    }

}
