<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\Kitchen;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $kitchens = Kitchen::all();
//        $dishNo = $kitchens->dishes->count();
//        dd($dishNo);
//        dd($kitchens);
        $no_of_orders = Order::all()->count();
        return view('home', [ 'kitchens' => $kitchens, 'ordersNo'=>$no_of_orders]);
    }
}
