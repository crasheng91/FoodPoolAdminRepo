<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    //
//    protected $table = 'dishes';

    public function kitchen(){
        return $this->belongsTo(Kitchen::class);
    }

    public function orders(){
        return $this->belongsToMany(Order::class);
    }
}
